<?php
session_start();
if(!isset($_SESSION['username'])){
  echo "<script type=text/javascript>
  alert('Anda Belum Login!!');
  window.location='../index.php';</script>";
}
?>               <!-- end row -->
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/s.png">
        <!-- App title -->
        <title>Inventaris SMK</title>

        <!-- DataTables -->
        <link href="../plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>


        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../plugins/switchery/switchery.min.css">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

      </head>
      <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
          <div id="status">
            <div class="spinner">
              <div class="spinner-wrapper">
                <div class="rotator">
                  <div class="inner-spin"></div>
                  <div class="inner-spin"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

          <!-- Top Bar Start -->
          <div class="topbar">

            <!-- LOGO -->
            <div class="topbar-left">
             <a href="index.html" class="logo"><span>Inven<span>taris</span></span><i class="mdi mdi-cube"></i></a>
             <!-- Image logo -->
             <!--<a href="index.html" class="logo">-->
             <!--<span>-->
             <!--<img src="assets/images/logo.png" alt="" height="30">-->
             <!--</span>-->
             <!--<i>-->
             <!--<img src="assets/images/logo_sm.png" alt="" height="28">-->
             <!--</i>-->
             <!--</a>-->
           </div>

           <!-- Button mobile view to collapse sidebar menu -->
           <div class="navbar navbar-default" role="navigation">
            <div class="container">

              <!-- Navbar-left -->
              <ul class="nav navbar-nav navbar-left">
                <li>
                  <button class="button-menu-mobile open-left waves-effect waves-light">
                    <i class="mdi mdi-menu"></i>
                  </button>
                </li>
                
                
              </ul>

              <!-- Right(Notification) -->
              <ul class="nav navbar-nav navbar-right">

                <li class="dropdown user-box">
                  <a href="" class="dropdown-toggle waves-effect waves-light user-link" data-toggle="dropdown" aria-expanded="true">
                    <img src="assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle user-img">
                  </a>

                  <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                    <li>
                      <h5>Hi, <?php echo $_SESSION['username']; ?></h5>
                    </li>
                       </li>
                                    <?php 
            include '../koneksi.php';
              $a=$_SESSION['username'];
              $q=mysqli_query($koneksi,"SELECT * FROM petugas where username='$a'");

              $r=mysqli_fetch_array($q);
                $id_pegawai=$r['id_petugas'];
            ?>
                                    <li><a href="edit_akun.php?id_petugas=<?php echo $r['id_petugas']; ?>"><i class="ti-settings m-r-5"></i> Pengaturan Akun</a></li>
                                   
                    <li><a href="logout.php"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                  </ul>
                </li>

              </ul> <!-- end navbar-right -->

            </div><!-- end container -->
          </div><!-- end navbar -->
        </div>
        <!-- Top Bar End -->


        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
          <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
              <div class="user-details">
                <div class="overlay"></div>
                <div class="text-center">
                  <img src="assets/images/users/avatar-1.jpg" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                  <div>
                    <a href="#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?php echo $_SESSION['username']; ?><span class=""></span></a>
                  </div>
                </div>
              </div>

              

              <ul>
                <li class="menu-title">Navigation</li>
                
                            <li class="has_sub">
                                <a href="index.php" class="waves-effect"><i class="mdi mdi-home"></i><span> Beranda </span> </a>
                               
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-border-all"></i><span> Inventaris <span class="menu-arrow"></span> </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="inventaris.php">Barang</a></li>
                                    <li><a href="barang.php">Jenis & Ruang</a></li>
                                    
                                </ul>
                            </li>
              
               <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-swap-vertical"></i><span> Transaksi <span class="menu-arrow"></span> </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="pinjam.php">Peminjaman</a></li>
                                    <li><a href="kembali.php">Pengembalian</a></li>
                                    
                                </ul>
                            </li>
              
              <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account-multiple"></i><span> Users <span class="menu-arrow"></span> </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="petugas.php">Petugas</a></li>
                                    <li><a href="pegawai.php">Pegawai</a></li>
                                    
                                </ul>
                            </li>
              
              

                            <li class="has_sub">
                                <a href="laporan.php" class="waves-effect"><i class="mdi mdi-file"></i><span> Laporan </span> </a>
                               
                            </li>

                            <li class="has_sub">
                                <a href="backup_db.php" class="waves-effect"><i class="mdi mdi-database"></i><span> Backup </span> </a>
                               
                            </li>
                


                

                
              </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

            

          </div>
          <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->



        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
          <!-- Start content -->
          <div class="content">
            <div class="container">


              <div class="row">
                <div class="col-xs-12">
                  <div class="page-title-box">
                    <h4 class="page-title">&nbsp </h4>
                    <ol class="breadcrumb p-0 m-0">

                    </ol>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
              
              <div class="row">
                
                   <div class="card-box table-responsive">

              <div class="col-lg-4">
               <div class="panel panel-color panel-info">
                <div class="panel-heading">
                  <h3 class="panel-title">Form Pinjam Barang</h3>
                </div>
                <div class="panel-body">
                  <form action="proses_pinjam.php" method="post"> 
                   <div class="form-group">
                    <input type="hidden" class="form-control"  name="id" >
                  </div>

                  <div class="form-group">

                  <label for="exampleInputPassword1">Nama Barang</label>

                    <select class="form-control"  name="id_inventaris"  required="">
                      <option value="">--- Silahkan Cari ---</option>
                      
                      <?php
                      include_once "../koneksi.php";
                      $tampil=mysqli_query($koneksi,"SELECT * FROM inventaris ORDER BY id_inventaris");
                      while($r=mysqli_fetch_array($tampil)){
                        ?>
                        <option value="<?php echo $r['id_inventaris']?>"> <?php echo $r['nama'] ?> | <?php echo "(stock : ".$r['jumlah'].")" ?></option>
                        <?php
                      }
                      ?>

                    </select>

                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Jumlah Pinjam</label>
                    <input type="number" class="form-control" id="itemterjual" name="jumlah_pinjam" placeholder="Jumlah Pinjam" autocomplete="off" required>
                  </div>

                   

                  <button type="submit" class="btn btn-inverse waves-effect waves-light">Tambah</button>
                  <button type="reset" class="btn btn-default waves-effect waves-light">Reset</button>
                </form>
              </div>
            </div>
          </div>
          


              <div class="col-lg-8">
               <div class="panel panel-color panel-info">
                <div class="panel-heading">
                  <h3 class="panel-title">Data Sementara</h3>
                </div>
                 <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="demo-box p-b-0">
                                               
                                            </div>

                                            <div class="row">
											 <?php 
                $status_peminjaman="pinjam";

                include "../koneksi.php";
                $query ="SELECT max(kode_pinjam) as maxKode FROM peminjaman";
                $hasil = mysqli_query($koneksi,$query);
                $data = mysqli_fetch_array($hasil);
                $kode_barang = $data['maxKode'];
                $noUrut = (int) substr($kode_barang, 4, 4);
                $noUrut++;
                $char = "PNJ";
                $kode_barang = $char . sprintf("%04s", $noUrut);
                ?>
                                                <div class="col-sm-4 col-xs-12">
												 <?php $tanggal_pinjam=gmdate("Y-m-d H:i:s",time()+60*60*7); 
                          
                          $status_peminjaman='Pinjam';
                          $status="Y";
                          $auto=mysqli_query($koneksi,"select * from peminjaman order by id_peminjaman desc limit 1");
                          $no=mysqli_fetch_array($auto);
                          $angka=$no['id_peminjaman']+1;  ?>
                                                    <div class="demo-box">
                                                        <form action="proses_transaksi.php" method="POST">
                                                            <div class="form-group">
                                                               
                                                                <input type="hidden" name="id_peminjaman"  class="form-control"><label>Tanggal Pinjam</label>
                                                        <input type="text" name="tanggal_pinjam" placeholder="" class="form-control" value="<?php echo $tanggal_pinjam;?>" readonly>
								                        <input type="hidden" name="tanggal_kembali" placeholder="" class="form-control">
                                                                <input type="hidden" name="status_peminjaman" class="form-control" value="pinjam" readonly>
                              
                                                                 
                                                                
                                                            </div>
                                                       
                                                    </div>
                                                </div>
												
												 <div class="col-sm-4 col-xs-12">
                                                    <div class="demo-box">
                                                       
                                                            <div class="form-group">
                                                                
                                                               <label>Kode Pinjam</label>
                                                                <input type="text" name="kode_pinjam" placeholder="" class="form-control" value="<?php echo $kode_barang;?>" readonly>
                                                            </div>
                                                           
                                                       
                                                    </div>
                                                </div>

                                                 

                                                <div class="col-sm-4 col-xs-12">
                                                    <div class="demo-box">
                                                       
                                                            <div class="form-group">
                                                                
                                                               <label>Peminjaman</label>
                                                                 <select class="form-control"  name="id_pegawai"  required="">
              <option value="">--- Silahkan Cari ---</option>

              <?php
              include_once "../koneksi.php";
              $tampil=mysqli_query($koneksi,"SELECT * FROM pegawai ORDER BY id_pegawai desc");
              while($r=mysqli_fetch_array($tampil)){
                ?>
                <option value="<?php echo $r['id_pegawai']?>"> <?php echo $r['nama_pegawai'] ?></option>
                <?php
            }
            ?>

        </select>
                                                            </div>
                                                           
                                                       
                                                    </div>
                                                </div>


                                              




                <div class="panel-body">
                 <div class="col-lg-12">

                  <div class="demo-box">
                   
                   <div class="table-rep-plugin">
                    <div class="table-responsive" data-pattern="priority-columns">
                      <table id="tech-companies-1" class="table  table-striped">

                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Barang</th>
                          <th>Jumlah Pinjam</th>
                          <th>Cancel</th>
                        </tr>
                      </thead>
                      <tbody>
                       <?php
					   $no=1;
                                    include "../koneksi.php";
                                    $select=mysqli_query($koneksi,"select * from data_sementara left join  
                                        inventaris on data_sementara.id_inventaris=inventaris.id_inventaris");
                                    while($r=mysqli_fetch_array($select))
                                    {
                          ?>
                      
                        <tr>
                          <th scope="row"><input name="" type="text" class="form-control" value="<?php echo $no++;?>" required="" readonly>
						  <input name="id_detail_pinjam[]" type="hidden" class="form-control" value="<?php echo $r['id_detail_pinjam'];?>" autocomplete="off"  required="" readonly>
                          <input name="status[]" type="hidden" class="form-control" value="<?php echo $status; ?>" autocomplete="off"  required="" readonly>
						  <input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $angka;?>" >
                          <input name="id_inventaris[]" type="hidden" class="form-control" value="<?php echo $r['id_inventaris'];?>" autocomplete="off"  required="" readonly>
                          </td></th>

                          <td><input name="" type="text" class="form-control" value="<?php echo $r['nama'];?>" autocomplete="off"  required="" readonly>
                          </td>
                          <td>
						  <input name="jumlah[]" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $r['jumlah'];?>"  >
						  <input name="jumlah_pinjam[]" type="text" class="form-control" value="<?php echo $r['jumlah_pinjam'];?>" autocomplete="off"  required="" readonly></td>
                          <td align="center"><a href="ds_hapus.php?id_inventaris=<?php echo $r['id_inventaris']; ?>" onclick="return confirm('Apakah Anda Yakin Ingin cancel');" class="table-action-btn h3"><i class="mdi mdi-close-box-outline text-danger"></i></a></td>
                        </tr>
                     <?php 
                   }
                   ?>
                      </tbody>
                    </table><br>
                       <button type="submit" name="pbarang" class="btn btn-inverse waves-effect waves-light">Pinjam</button></form>
                                            </div> <!-- end row -->
                  </div>

                    </div>
              </div>
            </div>
			
			
			
          </div>
          </div>
          </div>
          </div>
          </div>
</div>
</div>


 <div class="row">
                          
                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title"><b>Data Peminjaman</b></h4><br>
                                   
                                    <table id="datatable" class="table table-striped table-colored table-info">
                                        <thead>
                                        <tr>
                                           <th>No</th>
                                                  <th>Kode Pinjam</th>
                                                  <th>Tanggal Pinjam</th>
                                                  <th>Nama Pegawai</th>
                                                  <th>Status Peminjaman</th>
                                                   <th>Aksi</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                      <?php
                                           include "../koneksi.php";
                                           $select=mysqli_query($koneksi,"select * from peminjaman s left join pegawai p on p.id_pegawai=s.id_pegawai
                                             where status_peminjaman='Pinjam' order by id_peminjaman desc");
                                           $no=1;
                                           while($data=mysqli_fetch_array($select))
                                           {
                                            ?>
                                            <tr>
                                             <td><?php echo $no++; ?></td>
                                             <td><?php echo $data['kode_pinjam'];?></td>
                                             <td><?php echo $data['tanggal_pinjam']; ?></td>
                                             <td><?php echo $data['nama_pegawai']; ?></td>
                                             <td><span class="label label-danger"><?php echo $data['status_peminjaman']; ?></td>
                                          <td> 
										  <a href="detail_pinjam.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>" class="btn btn-inverse waves-light waves-effect w-md"><i class="fa fa-eye"></i> Detail</a>
                                                </td>
                                        </tr>
                                        
                                        </tbody>
                                        <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                            </div>
                        </div>




</div>

        </div> <!-- container -->

      </div> <!-- content -->

      <footer class="footer text-right">
        2019 © Ujikom Inventaris.
      </footer>


    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
      <a href="javascript:void(0);" class="right-bar-toggle">
        <i class="mdi mdi-close-circle-outline"></i>
      </a>
      <h4 class="">Settings</h4>
      <div class="setting-list nicescroll">
        <div class="row m-t-20">
          <div class="col-xs-8">
            <h5 class="m-0">Notifications</h5>
            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
          </div>
          <div class="col-xs-4 text-right">
            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
          </div>
        </div>

        <div class="row m-t-20">
          <div class="col-xs-8">
            <h5 class="m-0">API Access</h5>
            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
          </div>
          <div class="col-xs-4 text-right">
            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
          </div>
        </div>

        <div class="row m-t-20">
          <div class="col-xs-8">
            <h5 class="m-0">Auto Updates</h5>
            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
          </div>
          <div class="col-xs-4 text-right">
            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
          </div>
        </div>

        <div class="row m-t-20">
          <div class="col-xs-8">
            <h5 class="m-0">Online Status</h5>
            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
          </div>
          <div class="col-xs-4 text-right">
            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
          </div>
        </div>
      </div>
    </div>
    <!-- /Right-bar -->

  </div>
  <!-- END wrapper -->
 <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap.js"></script>

        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>
        <script src="../plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="../plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="../plugins/datatables/dataTables.scroller.min.js"></script>
        <script src="../plugins/datatables/dataTables.colVis.js"></script>
        <script src="../plugins/datatables/dataTables.fixedColumns.min.js"></script>

        <!-- init -->
        <script src="assets/pages/jquery.datatables.init.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script>
            $(document).ready(function () {
                $('#datatable').dataTable();
                $('#datatable-keytable').DataTable({keys: true});
                $('#datatable-responsive').DataTable();
                $('#datatable-colvid').DataTable({
                    "dom": 'C<"clear">lfrtip',
                    "colVis": {
                        "buttonText": "Change columns"
                    }
                });
                $('#datatable-scroller').DataTable({
                    ajax: "../plugins/datatables/json/scroller-demo.json",
                    deferRender: true,
                    scrollY: 380,
                    scrollCollapse: true,
                    scroller: true
                });
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
                var table = $('#datatable-fixed-col').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: false,
                    fixedColumns: {
                        leftColumns: 1,
                        rightColumns: 1
                    }
                });
            });
            TableManageButtons.init();

        </script>

  
  
</body>
</html>



