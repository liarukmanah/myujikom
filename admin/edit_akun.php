

  <?php
session_start();
if(!isset($_SESSION['username'])){
    echo "<script type=text/javascript>
    alert('Anda Belum Login!!');
    window.location='../index.php';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/s.png">
        <!-- App title -->
        <title>Inventaris SMK</title>

        <!-- App css -->
        <link href="../admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../admin/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="../admin/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="../admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../admin/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../admin/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="../admin/assets/css/responsive.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../plugins/switchery/switchery.min.css">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="../admin/assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                  <div class="spinner-wrapper">
                    <div class="rotator">
                      <div class="inner-spin"></div>
                      <div class="inner-spin"></div>
                    </div>
                  </div>
                </div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>Inven<span>taris</span></span><i class="mdi mdi-cube"></i></a>
                    <!-- Image logo -->
                    <!--<a href="index.html" class="logo">-->
                        <!--<span>-->
                            <!--<img src="assets/images/logo.png" alt="" height="30">-->
                        <!--</span>-->
                        <!--<i>-->
                            <!--<img src="assets/images/logo_sm.png" alt="" height="28">-->
                        <!--</i>-->
                    <!--</a>-->
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Navbar-left -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>
                           
                            
                        </ul>

                        <!-- Right(Notification) -->
                        <ul class="nav navbar-nav navbar-right">


                           

                            <li class="dropdown user-box">
                                <a href="" class="dropdown-toggle waves-effect waves-light user-link" data-toggle="dropdown" aria-expanded="true">
                                    <img src="../admin/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle user-img">
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                    <li>
                                        <h5>Hi, <?php echo $_SESSION['username']; ?></h5>
                                    </li>
                                    <?php 
            include '../koneksi.php';
              $a=$_SESSION['username'];
              $q=mysqli_query($koneksi,"SELECT * FROM petugas where username='$a'");

              $r=mysqli_fetch_array($q);
                $id_pegawai=$r['id_petugas'];
            ?>
                                    <li><a href="edit_akun.php?id_petugas=<?php echo $r['id_petugas']; ?>"><i class="ti-settings m-r-5"></i> Pengaturan Akun</a></li>
                                   
                                    <li><a href="logout.php"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul>
                            </li>

                        </ul> <!-- end navbar-right -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <div class="user-details">
                            <div class="overlay"></div>
                            <div class="text-center">
                                <img src="../admin/assets/images/users/avatar-1.jpg" alt="" class="thumb-md img-circle">
                            </div>
                            <div class="user-info">
                                <div>
                                    <a href="#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?php echo $_SESSION['username']; ?> <span class=""></span></a>
                                </div>
                            </div>
                        </div>

                        

                          <ul>
                            <li class="menu-title">Navigation</li>
                        
                            <li class="has_sub">
                                <a href="index.php" class="waves-effect"><i class="mdi mdi-home"></i><span> Beranda </span> </a>
                               
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-border-all"></i><span> Inventaris <span class="menu-arrow"></span> </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="inventaris.php">Barang</a></li>
                                    <li><a href="barang.php">Jenis & Ruang</a></li>
                                    
                                </ul>
                            </li>
                            
                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-swap-vertical"></i><span> Transaksi <span class="menu-arrow"></span> </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="pinjam.php">Peminjaman</a></li>
                                    <li><a href="kembali.php">Pengembalian</a></li>
                                    
                                </ul>
                            </li>
                            
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account-multiple"></i><span> Users <span class="menu-arrow"></span> </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="petugas.php">Petugas</a></li>
                                    <li><a href="pegawai.php">Pegawai</a></li>
                                    
                                </ul>
                            </li>
                            
                            

                            <li class="has_sub">
                                <a href="laporan.php" class="waves-effect"><i class="mdi mdi-file"></i><span> Laporan </span> </a>
                               
                            </li>

                            <li class="has_sub">
                                <a href="backup_db.php" class="waves-effect"><i class="mdi mdi-database"></i><span> Backup </span> </a>
                               
                            </li>
                            

                            
                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                   

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">&nbsp</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    


                                    <div class="row"><h4 class="m-t-0 header-title"><b>Ubah Password</b></h4>
                                        <div class="col-md-12 m-t-50">
                                         
                                            <form action="update_akun.php" method="post"> 
                                                
                                                <div class="form-group">
                                                    <label for="exampleInputText">Password</label>
                                                     <input type="hidden" name="id_petugas" value="<?php echo $r['id_petugas']; ?>" class="form-control" id="nama" placeholder="Masukan nama" autocomplete="off" required="">
                                                    <input type="text" name="password" class="form-control"  placeholder="*******" autocomplete="off" >
                                                </div>

                                                
                                               

                                             

                                                
                                                <button type="submit" name="submit" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-save"></i> Simpan</button>
                                                <a href="index.php" class="btn btn-default waves-effect waves-light"><i class="fa fa-close"></i> Batal</a>
                                            </form>
                                        </div>

                            </div>
                        </div>
                        <!-- end row -->




                    </div> <!-- container -->

                </div> <!-- content -->

                  <footer class="footer text-right">
                    2019 © Ujikom Inventaris.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../admin/assets/js/jquery.min.js"></script>
        <script src="../admin/assets/js/bootstrap.min.js"></script>
        <script src="../admin/assets/js/detect.js"></script>
        <script src="../admin/assets/js/fastclick.js"></script>
        <script src="../admin/assets/js/jquery.blockUI.js"></script>
        <script src="../admin/assets/js/waves.js"></script>
        <script src="../admin/assets/js/jquery.slimscroll.js"></script>
        <script src="../admin/assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <script type="text/javascript" src="../plugins/parsleyjs/parsley.min.js"></script>

        <!-- App js -->
        <script src="../admin/assets/js/jquery.core.js"></script>
        <script src="../admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
			$(document).ready(function() {
				$('form').parsley();
			});
            $(function () {
                $('#demo-form').parsley().on('field:validated', function () {
                    var ok = $('.parsley-error').length === 0;
                    $('.alert-info').toggleClass('hidden', !ok);
                    $('.alert-warning').toggleClass('hidden', ok);
                })
                .on('form:submit', function () {
                    return false; // Don't submit form for this demo
                });
            });
		</script>

    </body>
</html>