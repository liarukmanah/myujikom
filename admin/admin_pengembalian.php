
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
    #line-chart {
        height:300px;
        width:800px;
        margin: 0px auto;
        margin-top: 1em;
    }
    .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
        color: #fff;
    }
</style>

<script type="text/javascript">
    $(function() {
        var uls = $('.sidebar-nav > ul > *').clone();
        uls.addClass('visible-xs');
        $('#main-menu').append(uls.clone());
    });
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!-- Le fav and touch icons -->
  <link rel="shortcut icon" href="../assets/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
      <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
          <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
              <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
                  <!--[if (gt IE 9)|!(IE)]><!--> 
                  
                  <!--<![endif]-->

                  <div class="navbar navbar-default" role="navigation" style="background: #008080 ;">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="" href="index.php"><span class="navbar-brand"><span class="fa fa-briefcase"></span> Inventaris Sarana dan Prasarana</span></a></div>

                    <div class="navbar-collapse collapse" style="height: 1px;">
                      <ul id="main-menu" class="nav navbar-nav navbar-right">
                        <li class="dropdown hidden-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i> 
                                <i class="fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">


                                <li>
                              <a href="#myModal" role="button" data-toggle="modal">
                                  <i class="ace-icon fa fa-power-off"></i>
                                  Logout
                              </a>
                          </li>
                      </ul>
                  </li>
              </ul>

          </div>
      </div>
  </div>


 <div class="content">
        <div class="header">
            

            <h1 class="page-title">Pengembalian</h1>
                    <ul class="breadcrumb">
           
        </ul>

        </div>
        <div class="main-content">

    </div>  
              <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                            Form Kembali
                        </div>
                        <div class="panel-body">
                        <center><div class="panel-body">
                        <div class="col-lg-3 col-md-offset-4">
                <label>Pilih Kode Peminjaman</label>
                <form method="GET">
              
                    <select name="id_peminjaman" class="form-control m-bot15">
                        <?php
                        include "koneksi.php";
                                //display values in combobox/dropdown
                        $result = mysql_query("SELECT id_peminjaman,kode_pinjam from peminjaman where status_peminjaman='Pinjam' ");
                        while($row = mysql_fetch_assoc($result))
                        {
                            echo "<option value='$row[id_peminjaman]'>$row[kode_pinjam]</option>";
                        } 
                        ?>
                   </select>
                                    <br/>
                                <button type="submit" name="pilih" class="btn btn-outline btn-primary">Tampilkan</button>
                            </form>
                        </div>
                    </div></center>

                <?php
                if(isset($_GET['pilih'])){?>
                  <form action="hapus_pnj.php" method="post" enctype="multipart/form-data">
                     <?php
                     include "../koneksi.php";
                     $id_peminjaman=$_GET['id_peminjaman'];
                     $select=mysql_query("select * from peminjaman i left join detail_pinjam p on p.id_detail_pinjam=i.id_peminjaman 
                      left join inventaris v on p.id_inventaris=v.id_inventaris
                        where id_peminjaman='$id_peminjaman' AND status='Y'");

                     $nomor=mysql_fetch_array(mysql_query("select * from peminjaman left join pegawai on pegawai.id_pegawai=peminjaman.id_pegawai where id_peminjaman='$id_peminjaman'"));
                        ?>
            <br>
            <br>
            <br>
            <?php 
            while($r=mysql_fetch_array($select)){

              ?>
  <div class="col-md-2">Kode Pinjam<input name="kode_pinjam" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $nomor['kode_pinjam'];?>" autocomplete="off" maxlength="11" readonly>

    <input name="id_inventaris[]" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $r['id_inventaris'];?>" autocomplete="off" maxlength="11"></div> 

    <div class="col-md-2">Nama Barang<input name="" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $r['nama'];?>" autocomplete="off" maxlength="11" readonly></div>
    <input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $r['id_peminjaman'];?>" autocomplete="off" maxlength="11">
    <input name="id[]" type="hidden" class="form-control" placeholder="="Masukan ID" value="<?php echo $r['id'];?>" autocomplete="off" maxlength="11">
   <input name="id_detail_pinjam[]" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $r['id_detail_pinjam'];?>" autocomplete="off" maxlength="11">
  <div class="col-md-3">Tanggal Pinjam<input name="tanggal_pinjam" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $nomor['tanggal_pinjam'];?>" autocomplete="off" maxlength="11" readonly>
   <input name="tanggal_kembali" type="hidden"> </div>
 <div class="col-md-2">Nama Pegawai<input name="id_pegawai" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $nomor['id_pegawai'];?>.<?php echo $nomor['nama_pegawai'];?>" autocomplete="off" maxlength="11" readonly>
<input type="hidden" name="status_peminjaman" value="<?php echo $r['status_peminjaman']?>" ></div>
 <div class="col-md-2">Jumlah Pinjam<input type="text" id="jumlah" name="jumlah[]" value="<?php echo $r['jumlah_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah" readonly>
   <input type="hidden" name="status[]" value="<?php echo $r['status'];?>" class="form-control col-md-7 col-xs-12" placeholder="tanggal kembali">
   </div>
   <div>
    <br>
  <input type="checkbox" name="kembali[]" value="<?php echo $r['id'];?>">&nbsp;Pilih</input>
   </div>
   <br><br>

        <?php } ?>
        <br>

      &nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-warning">&nbsp;Kembalikan</button>

      </form>
              
          

<?php } ?>
                     
                        <br>
                        <br>
                        
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <div class="table-responsive">

                                 <table id="example" class="table table-striped  table-colored table-info">
                                    <thead>
                                       <tr class="success">
                                            <th>No</th>
                                            <th>Kode Pinjam</th>
                                            <th>Tanggal Pinjam</th>
                                            <th>Tanggal Kembali</th>
                                            <th>Status Peminjaman</th>
                                            <th>Nama Pegawai</th>
                                            <th>Option</th>
                                    
                                        </tr> 
                                    </thead>
                                    <tbody>
                                    <?php
                                    include "../koneksi.php";
                                    $no=1;
                                    $select=mysql_query("select * from peminjaman left join pegawai  on pegawai.id_pegawai=peminjaman.id_pegawai where status_peminjaman='kembali'");
                                    while($data=mysql_fetch_array($select))
                                    {
                                    ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $data['kode_pinjam']; ?></td>
                                            <td><?php echo $data['tanggal_pinjam']; ?></td>
                                            <td><?php echo $data['tanggal_kembali']; ?></td>
                                            <td><?php echo $data['status_peminjaman']; ?></td>
                                            <td><?php echo $data['nama_pegawai']; ?></td>

                                            
                                       
                                        <td>
                                       <a class="btn btn outline btn-danger fa fa-trash-o" href="hapus_pengembalian.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>"></a></td>
                                       </tr>
                                        <?php
                                    }
                                    ?>
                                     
                                            
                                            
                                    </tbody>
                                </table>
                                 <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Logout Confirmation</h3>
        </div>
       <div class="modal-body">
            <p class="error-text"><i class="fa fa-warning modal-icon"></i> yakin ingin keluar dari webiste ini?
        </div>
        Silahkan Klik Button Logout
        <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Kembali</button>
            <a class="btn btn-warning" href="logout.php">Logout</a>
           
        </div>
      </div>
    </div>
</div>
                            <script type="text/javascript" src="assets/js/jquery.js"></script>
                                <script type="text/javascript" src="assets/js/jquery.min.js"></script>
                                <script type="text/javascript" src="assets/js/jquery.dataTables.js"></script>
                                <script type="text/javascript" src="assets/js/jquery.dataTables.min.js">
                                    
                                </script>
                            
                                <script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
                            </script>



            <footer>
                <hr>

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p class="pull-right">Inventaris</a> 
                <p> © 2018 Sarana & Prasarana</a></p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
</body>

</html>







