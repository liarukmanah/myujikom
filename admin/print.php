<?php
include '../koneksi.php';
require('assets/pdf/fpdf.php');

class myPDF extends FPDF {
    function myCell($w,$h,$x,$t){
        $height=$h/3;
        $first=$height+2;
        $second=$height+$height+$height+3;
        $len=strlen($t);
        if($len>15){
            $txt=str_split($t,15);
            $this->SetX($x);
            $this->Cell($w,$first,$txt[0],'','','');
            $this->SetX($x);
            $this->Cell($w,$second,$txt[1],'','','');
            $this->SetX($x);
            $this->Cell($w,$h,'','LTRB',0,'L',0);
        }
        else{
            $this->SetX($x);
            $this->Cell($w,$h,$t,'LTRB',0,'L',0);
        }
    }
}
$pdf = new myPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',16);
$pdf->Ln();

$w=45;
$h=15;

$no=1;
$query=mysqli_query($koneksi,"select * from inventaris order by id_inventaris asc");
while($lihat=mysqli_fetch_array($query)){
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,'No');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,'Nama Barang');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,'Kondisi');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,'Keterangan');
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell($w,$h,$x, $no);
$x=$pdf->getx();
$pdf->myCell($w,$h,$x, $lihat['nama']);
$x=$pdf->getx();
$pdf->myCell($w,$h,$x, $lihat['kondisi']);
$x=$pdf->getx();
$pdf->myCell($w,$h,$x, $lihat['keterangan']);
$pdf->Ln();


$no++;
}

$pdf->Output("cetak_barang.pdf","I");

?>
